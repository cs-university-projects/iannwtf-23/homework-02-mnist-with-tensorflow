from typing import Optional

import matplotlib.pyplot as plt


def visualize(train_losses, train_accuracies, test_losses, test_accuracies, name: Optional[str] = None):
    """
    Plot the received metrics.
    """

    fig, ax = plt.subplots()
    plt.title("Loss and Accuracy over Training Epochs")

    plt.xlabel("Training Epoch")

    # Plotting average training and test losses
    train_loss_line, = ax.plot(train_losses, 'r:', label='Avg. Train Loss')
    test_loss_line, = ax.plot(test_losses, 'r-', label='Avg. Test Loss')
    ax.tick_params(axis='y', labelcolor='red')
    ax.set_ylabel('Loss', color='red')

    # Creating a second y-axis for accuracy
    ax2 = ax.twinx()
    train_acc_line, = ax2.plot([item * 100 for item in train_accuracies], 'b:', label='Train Accuracy (%)')
    test_acc_line, = ax2.plot([item * 100 for item in test_accuracies], 'b-', label='Test Accuracy (%)')
    ax2.tick_params(axis='y', labelcolor='blue')
    ax2.set_ylabel('Accuracy (%)', color='blue')

    # Adding a single legend for both axes
    lines = [train_loss_line, test_loss_line, train_acc_line, test_acc_line]
    labels = ['Avg. Train Loss', 'Avg. Test Loss', 'Train Accuracy (%)', 'Test Accuracy (%)']
    ax.legend(lines, labels, loc='upper left')

    try:
        plt.show()
    except ConnectionRefusedError:
        plt.imsave(f"./plots/{''.join(x for x in name if x in '-_.,()' or x.isalnum())}.png")