import logging

import numpy as np
import tensorflow as tf
from keras import Model, losses, optimizers


def train(model: Model,
          train_ds,
          test_ds,
          loss_func=losses.CategoricalCrossentropy(),
          optimizer=optimizers.SGD(),
          n_epochs: int = 10):
    """
    Train the model for n epochs and test in each epoch.

    Return lists with the overall train_losses, train_accuracies, test_losses, test_accuracies
    """

    test_losses = []
    train_losses = []
    test_accuracies = []
    train_accuracies = []

    for epoch in range(n_epochs):
        train_loss, train_accuracy = train_step(model, train_ds, loss_func, optimizer)
        train_losses.append(train_loss)
        train_accuracies.append(train_accuracy)

        test_loss, test_accuracy = test(model, test_ds, loss_func)
        test_losses.append(test_loss)
        test_accuracies.append(test_accuracy)

        print(f"Epoch {epoch}/{n_epochs} ({round(epoch/n_epochs*100, 2)}): "
              f"accuracy=(train: {round(float(train_accuracy)*100, 2)}%, test: {round(float(test_accuracy)*100, 2)}%) "
              f"loss=(train: {round(float(train_loss), 2)}, test: {round(float(test_loss), 2)})")


    return (train_losses,
            train_accuracies,
            test_losses,
            test_accuracies)


def train_step(model: Model,
               train_ds,
               loss_func=losses.CategoricalCrossentropy(),
               optimizer=optimizers.SGD()):
    """
    Train the network on the train data (corresponds to one training epoch).

    Return the average loss and the average accuracy
    """
    losses = []
    accuracies = []

    for (inp, target) in train_ds:
        with tf.GradientTape() as tape:
            pred = model(inp)
            loss = loss_func(pred, target)

        gradients = tape.gradient(loss, model.trainable_variables)
        optimizer.apply_gradients(zip(gradients, model.trainable_variables))

        # Calculate the current training accuracy:
        train_accuracy = np.mean(np.argmax(pred, axis=1) == np.argmax(target, axis=1))

        # Record our metrics:
        losses.append(loss)
        accuracies.append(train_accuracy)

    return tf.reduce_mean(losses), tf.reduce_mean(accuracies)


def test(model, test_ds, loss_func):
    test_losses = []
    test_accuracies = []
    """
    Test the network with the test data (corresponds to one training epoch).

    Return the average test losses and the average test accuracy
    """

    for (inp, target) in test_ds:
        pred = model(inp)
        loss = loss_func(pred, target)
        test_accuracy = np.mean(np.argmax(pred, axis=1) == np.argmax(target, axis=1))

        test_losses.append(loss)
        test_accuracies.append(test_accuracy)

    return tf.reduce_mean(test_losses), tf.reduce_mean(test_accuracies)

