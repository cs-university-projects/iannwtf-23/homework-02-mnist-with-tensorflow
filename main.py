import tensorflow as tf
import tensorflow_datasets as tfds
from keras import Model, layers
from tensorflow.python.client.session import Session
from tensorflow.python.proto_exports import ConfigProto

from src.training import train
from src.visualization import visualize


print("GPU is", "available" if tf.config.experimental.list_physical_devices("GPU") else "NOT AVAILABLE")


def prepare_data(d):
    # flatten the images into vectors
    d = d.map(lambda img, target: (tf.reshape(img, (-1,)), target))
    # convert data from uint8 to float32
    d = d.map(lambda img, target: (tf.cast(img, tf.float32), target))
    # sloppy input normalization, just bringing image values from range [0, 255] to [-1, 1]
    d = d.map(lambda img, target: ((img / 128.) - 1., target))
    # create one-hot targets
    d = d.map(lambda img, target: (img, tf.one_hot(target, depth=10)))
    # cache this progress in memory, as there is no need to redo it; it is deterministic after all mnist mnist.cache()
    # shuffle, batch, prefetch
    d = d.shuffle(1000)
    d = d.batch(32)
    d.prefetch(20)
    # return preprocessed dataset
    return d


class MyModel(Model):
    def __init__(self):
        super().__init__()
        self.dense1 = layers.Dense(256, activation=tf.nn.sigmoid)
        self.dense2 = layers.Dense(256, activation=tf.nn.sigmoid)
        self.out = layers.Dense(10, activation=tf.nn.softmax)

    @tf.function
    def call(self, x):
        x = self.dense1(x)
        x = self.dense2(x)
        return self.out(x)


if __name__ == "__main__":
    (train_ds, test_ds), ds_info = tfds.load('mnist', split=['train', 'test'], as_supervised=True, with_info=True)
    train_ds = prepare_data(train_ds)
    test_ds = prepare_data(test_ds)

    model = MyModel()

    train_losses, train_accuracies, test_losses, test_accuracies = train(model, train_ds, test_ds, n_epochs=3)
    visualize(
        train_losses,
        train_accuracies,
        test_losses,
        test_accuracies,
    )
