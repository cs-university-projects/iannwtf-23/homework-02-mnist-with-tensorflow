# Homework 02 – MNIST with Tensorflow

This is a neural network implemented in Tensorflow to classify images in the MNIST dataset.

## Results

| Configuration                                                                      | Result                                                                                                        |
|------------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------|
| 10 epochs<br/>Learning rate: 0.01<br/>Momentum: 0.0<br/>Hidden layers: 2x256       | ![result 1](./img/plot_10_epochs.jpg)                                                                         |
| 30 epochs<br/>Learning rate: 0.01<br/>Momentum: 0.0<br/>Hidden layers: 2x256       | ![result 1](./img/plot_30_epochs.png)                                                                         |
| 10 epochs<br/>Learning rate: 0.01<br/>Momentum: 0.0<br/>Hidden layers: 1x256, 1x100, 1x70 | ![result 1](./img/plot_10_epochs_100x70_units.jpg)                                                            |
| 10 epochs<br/>Learning rate: 0.005<br/>Momentum: 0.01<br/>Hidden layers: 2x256     | ![plot_10_epochs_.005_learning_rate_.01_momentum.jpg](img/plot_10_epochs_.005_learning_rate_.01_momentum.jpg) |

